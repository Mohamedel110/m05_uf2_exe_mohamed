
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;

public final class Password {

    private int longitud;
    private String contrasena;

    public Password() {
        longitud = 10;
        contrasena = "";
    }

    public Password(int longitud) throws ExcepcionPasswordNoValida, ExcepcionPasswordNoValida, ExcepcionPasswordNoValida {

        try {
            if (longitud >= 6) {
                if (longitud <= 10) {
                    this.longitud = longitud;
                }

            }

        } catch (NumberFormatException e) {
            try {
                throw new ExcepcionPasswordNoValida("La Contraseña debe contener entre 6 a 10 caracteres.");
            } catch (ExcepcionPasswordNoValida ex) {
                Logger.getLogger(Password.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

        this.contrasena = this.createPassword();
    }

    public Password(String contrasena) {
        this.longitud = contrasena.length();
        this.contrasena = contrasena;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }

    public int getLongitud() {

        return longitud;
    }

    public String getContrasena() {
        return contrasena;
    }

    public void setContrasena(String contrasena) {
        this.contrasena = contrasena;
    }

    public String createPassword() {

        final char[] caracteres
                = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P',
                    'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
                    'a', 'b', 'c', 'd', 'e', 'f', 'g', 'e', 'h', 'i', 'j', 'l', 'k', 'm',
                    'n', 'o', 'p', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
                    '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
                    '@', '#', '!', '$', '€', '&', '[', ']'};

        String temporal = "";

        Random aleatorio = new Random();

        for (int i = 0; i < this.longitud; i++) {

            temporal += caracteres[aleatorio.nextInt(caracteres.length)];

        }

        return temporal;
    }

    public boolean isStrong() {
        boolean fort = false;
        int contador;
        char clave;
        byte contNumero = 0, contLetraMay = 0, contLetraMin = 0;
        for (byte i = 0; i < contrasena.length(); i++) {
            clave = contrasena.charAt(i);
            String passValue = String.valueOf(clave);
            if (passValue.matches("[A-Z]")) {
                contLetraMay++;
                if (passValue.matches("[a-z]")) {
                    contLetraMin++;

                    if (passValue.matches("[0-9]")) {
                        contNumero++;
                        fort = true;
                    }
                }
            }
        }
        return fort;
    }

    @Override
    public String toString() {
        return "Longitud: " + this.longitud + " Password: " + this.contrasena;
    }
}
